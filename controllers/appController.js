/*var restful = require('node-restful');
module.exports = function(app, route) {

  // Setup the controller for REST;
  var rest = restful.model('app',app.models.app).methods(['get','put','post','delete']);

  //Resgister this endpoint with the application
  rest.register(app,route);

  // Return middleware.
  return function(req, res, next) {
    next();
  };
};*/
var Resource = require('resourcejs');
module.exports = function(app, route) {

  // Setup the controller for REST;
  Resource(app, '', route, app.models.app).rest();

  // Return middleware.
  return function(req, res, next) {
    next();
  };
};