const mongoose = require('mongoose');

// Create the MovieSchema.
var reviewSchema = new mongoose.Schema({
    userName: {
        type: String
    },
    date: {
        type: Date,
        required: true,
        default: Date.now
    },
    url: {
        type: String,
        required: true
    },
    score: {
        type: Number,
        required: true
    },
    title: {
        type: String
    },
    text: {
        type: String
    },
    sentiment: {
        type: mongoose.Schema.Types.Mixed
    },
    reviewId: {
        type: String,
        required: true,
        unique: true
    },
    app: { type: mongoose.Schema.Types.ObjectId, ref: 'app' }
});

// Export the model.
module.exports = mongoose.model('review', reviewSchema);