var myApp = angular.module('myApp',['ngRoute']);

myApp.config(function($routeProvider){
	$routeProvider.when('/', {
		controller:'SearchController',
		templateUrl: 'views/search.html'
	})
	.when('/search', {
		controller:'SearchController',
		templateUrl: 'views/search.html'
	})
    .when('/apps',{
		controller:'AppsController',
		templateUrl: 'views/apps.html'
	})
	.when('/apps/:id',{
		controller:'AppsController',
		templateUrl: 'views/app.html'
	})
	.when('/download/:id',{
		controller:'DownloadController',
		templateUrl: 'views/download.html'
	})
	.when('/fetchreviews/:id',{
		controller:'FetchreviewsController',
		templateUrl: 'views/fetchreview.html'
	})
	.otherwise({
		redirectTo: '/'
	});
});