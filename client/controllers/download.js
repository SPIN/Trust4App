var myApp = angular.module('myApp');

myApp.controller('DownloadController', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams){
	console.log('DownloadController loaded...');
	$scope.loading = false;

	$scope.getDownloadInfo = function(){
		angular.element('*[id^="tviz"]').remove();
		var id = $routeParams.id;
		$scope.loading = true;
		$http.get('/crawler/getDownloadInfo/'+id).then(function(response){
			$scope.app = response.data;
			$scope.loading = false;
		});
	}

	$scope.getReviews = function(){
		var id = $routeParams.id;
		$scope.loading = true;
		$http.get('/crawler/getReviews/'+id).then(function(response){
			$scope.app = response.data;
			$scope.loading = false;
		}).catch(function() {
			$scope.getDownloadInfo();
		});
	}

	$scope.getPermissions = function(){
		var id = $routeParams.id;
		$scope.loading = true;
		$http.get('/crawler/getPermissions/'+id).then(function(response){
			$scope.app = response.data;
			$scope.loading = false;
		});
	}

	$scope.getAppicaptorResult = function(){
		var id = $routeParams.id;
		$scope.loading = true;
		$http.get('/crawler/getAppicaptorResult/'+id).then(function(response){
			$scope.app = response.data;
			$scope.loading = false;
		});
	}

}]);