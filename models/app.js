const mongoose = require('mongoose');


var appicaptorSchema = new mongoose.Schema({
    overprivilegedPermission: {
        type: mongoose.Schema.Types.Mixed
    },
    dangerousPermission: {
        type: mongoose.Schema.Types.Mixed
    },
    extensiveTrackingFrameworkUsage: {
        type: mongoose.Schema.Types.Mixed
    },
    outdatedCryptographicPrimitives: {
        type: mongoose.Schema.Types.Mixed
    },
    unprotectedCommunication: {
        type: mongoose.Schema.Types.Mixed
    }
});

var permissionSchema = new mongoose.Schema({
    permission: {
        type: String,
        required: true
    },
    description: {
        type: String
    }
});

var appSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    summary: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    free: {
        type: Boolean,
        required: true
    },
    minInstalls: {
        type: Number,
        required: true
    },
    maxInstalls: {
        type: Number,
        required: true
    },
    score: {
        type: Number,
        required: true
    },
    reviews: [{ type: mongoose.Schema.Types.ObjectId, ref: 'review' }],
    developer: {
        type: mongoose.Schema.Types.Mixed
    },
    developerEmail: {
        type: String
    },
    developerWebsite: {
        type: String
    },
    updated: {
        type: Date,
        default: Date.now
    },
    version: {
        type: String
    },
    genre: {
        type: String,
        required: true
    },
    genreId: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    histogram: {
        type: mongoose.Schema.Types.Mixed
    },
    offersIAP: {
        type: Boolean,
        required: true
    },
    adSupported: {
        type: Boolean,
        required: true
    },
    androidVersionText: {
        type: String,
        required: true
    },
    androidVersion: {
        type: String,
        required: true
    },
    contentRating: {
        type: String,
        required: true
    },
    screenshots: [
        {
            type: String
        }
    ],
    comments: [
        {
            type: String
        }
    ],
    recentChanges: [
        {
            type: String
        }
    ],
    preregister: {
        type: Boolean,
        required: true
    },
    url: {
        type: String,
        required: true
    },
    appId: {
        type: String,
        required: true,
        unique: true
    },
    playstoreUrl: {
        type: String,
        required: true
    },
    reviewsCount: {//rename to ratingscount
        type: Number
    },
    permissions: [permissionSchema],
    //appicaptor: appicaptorSchema
    appicaptor: {
        type: mongoose.Schema.Types.Mixed
    }
});

// Export the model.
module.exports = mongoose.model('app', appSchema);