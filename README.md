# Introduction 
This project contains the Server, client as well as the Mobile app of the Trust4App framework. Trust4App calculates the trustworthiness of a mobile application considering the public information available on the Play Store as well as the security health of that app. 

#Prerequisites
Node.js(https://nodejs.org/en/)
nodemon(http://nodemon.io/)

#Installation
npm install

#running the server and the client
nodemon
url: http://localhost:3000/

#Proof-of-concept system can be found under http://130.83.163.83:3000/#!/apps . Click "Apps" on the right hand corner to browse the trustworthiness scores of different mobile apps.