var restful = require('node-restful');
module.exports = function(app, route) {

  // Setup the controller for REST;
  var rest = restful.model('permission',app.models.permission).methods(['get','put','post','delete']);

  //Resgister this endpoint with the application
  rest.register(app,route);

  // Return middleware.
  return function(req, res, next) {
    next();
  };
};