var myApp = angular.module('myApp');

myApp.controller('SearchController', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams){
	console.log('SearchController loaded...');

	$scope.data = {
		searchTerm: "",
		countryCheck: false
	};

	$scope.searchResults = [];

	$scope.search = function(){		
		angular.element('*[id^="tviz"]').remove();
		var extraParams = "&num=30";
		extraParams += ($scope.data.countryCheck) ? "&country=de" : "";
		$http.get('/crawler/apps/?q='+ $scope.data.searchTerm + extraParams).then(function(response){
			console.log(response.data);

			$scope.searchResults = response.data.results;
		});
	}

}]);